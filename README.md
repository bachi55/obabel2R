# obabel2R
Functions for calling openbabel executable inside R to do various conversions.
Opelbabel need to be installed on system and be in path to convert inchi, smile, sdf etc.
[ChemmineOB](https://github.com/girke-lab/ChemmineOB) can now be used instead for most operation. But it doesn't support inchi on windows at this moment (see [here](https://github.com/girke-lab/ChemmineOB/issues/9)).


#### Convert compound identifiers
* `inchi2sdf`,`smile2inchi`,`inchi2smile`
* `inchi.keep.cont`: Keep only largest continues part of a molecule InChI (AKA remove salts).
* `inchi.rem.charges`: Remove charges from a molecule InChI.
* `inchi.rem.stereo`: Remove stereochemistry from a molecule InChI.


## Installation
Install the devtools package and run:

```r
devtools::install_git("https://gitlab.com/R_packages/obabel2R.git")
```
